dns_domain = "halftown.co.uk"
dns_zone_id = "Z3C902YPFDHBD"

vpc_dmz_cidr = "172.28.0.0/16"
dmz_subnet_cidr = "172.28.1.0/24"

vpc_main_cidr = "172.29.0.0/16"
main_subnet_cidr = "172.29.1.0/24"

app_ec2_instance_type = "t2.nano"
rds_ec2_instance_type = "db.t2.micro"
